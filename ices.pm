# At least ices_get_next must be defined. And, like all perl modules, it
# must return 1 at the end.

# Function called to initialize your python environment.
# Should return 1 if ok, and 0 if something went wrong.

#use utf8;

sub ices_init {
	print "Perl subsystem Initializing:\n";
	use Mysql;
	#use MP3::Info;
	use utf8;
	return 1;
}

# Function to remove any beginning and ending white spaces
# Credits goes to: can't find right now (internet)
sub trim {
   my($string)=@_;
   for ($string) {
       s/^\s+//;
       s/\s+$//;
   }
   return $string;
}

# Function to add backslashes to filenames' special chars
# Chars: \,',",(,),\0
# Credits goes to: can't find right now (internet)
sub AddSlashes {
   $text = shift;
   ## Make sure to do the backslash first!
   $text =~ s/\\/\\\\/g;
   $text =~ s/'/\\'/g;
   $text =~ s/"/\\"/g;
   $text =~ s/\(/\\\(/g;
   $text =~ s/\)/\\\)/g;
   #$text =~ s/ /\\ /g;
   $text =~ s/\\0/\\\\0/g;
   return $text;
}

# Function called to shutdown your python enviroment.
# Return 1 if ok, 0 if something went wrong.
sub ices_shutdown {
	print "Perl subsystem shutting down:\n";
}

# Function called to get the next filename to stream. 
# Should return a string.
sub ices_get_next {
	# Files and directories settings
	$base = '/500gb/autopilot/';
	$adverts = 'reklamy';
	$advertDir = $base.$adverts;
	$fallback = 'archiwum';
	$logfile = '/tmp/ices.log';
	
	# Other settings
	$normalTail = '10';
	$advertTail = '7';
	
	# MySQL settings
	$host = "localhost";
	$database = "dbname";
	$tablename = "table_name";
	$user = "username";
	$pw = "hackme";
	
	eval{
		$connect = Mysql->connect($host, $database, $user, $pw);
		$connect->selectdb($database);
		
		@timeData = localtime(time);
		$curHour = $timeData[2]+$timeData[1]/60;
		$curDay = $timeData[6];
		
		# Check if it's time to play from a directory
		# and which one it should be
		$myquery = "SELECT katalog,kolejno FROM `$tablename` WHERE `dzientyg` LIKE '%$curDay%' AND poczatekh+poczatekm/60 <= $curHour AND koniech+koniecm/60 > $curHour LIMIT 1";
		
		$execute = $connect->query($myquery);
		@result = $execute->fetchrow();
		
		# Get the first result...
		$dirName = $result[0];
		# ... and make sure it's a valid dir name
		$dirName =~ s/([;<>\*\|`&\$!#\(\)\[\]\{\}:'"])/\\$1/g;
		print "dirName: $dirName\n";
		
		# Remember the list ordering
		$order = $result[1];
		print "Order: $order\n";
		
		# Let's find out if the directory exists or not
		if (-d $base.$result[0]){
		   print "Should play from directory: ".$dirName."\n";
		   $base = $base.$dirName;
		}
		else{
		   print "Should play from: ".$dirName.", but the directory does not exist!\n";
		}
	};
	
	# If the eval went wrong we should use the fallback directory
	if($@){
		print "There was a problem with database connection. Using fallback directory.\n";
		$base = $base.$fallback;
	}
	
	# Should we play an advertisment or a normal title
	#TODO: rand with variable
	if (rand()<0.25 && $order!=1){
		# If yes - change the base dir to /base/nowplaying/advertdir
		$base = $base.$adverts.'/';
		# If the directory does not exist set base dir to default advert dir
		unless (-d $base) {
		   $base = $advertDir;
		}
		# Exclude last n played adverts
		@excluded = AddSlashes(`sed 's/Playing //' $logfile | grep reklamy | tail -n $advertTail | tr "\\n" "|"`);
	}
	else{
		# Exclude last n played files
		@excluded = AddSlashes(`sed 's/Playing //' $logfile | tail -n $normalTail | tr "\\n" "|"`);   
	}
	$ex = substr($excluded[0],0,-1);
	#print "Expression: $ex\n";
	
	# If we are in main dir we look for all files in directories without a dot at the beginning
	# Example: /500gb/autopilot/somedir/.someotherdir/* (will be hidden)
	if(!$dirName){
	   @listFiles = `find '$base' -type f | egrep 'mp3|wma|MP3|WMA|ogg|OGG|m4a|M4A' | grep -v '/\\.' | egrep -v "$ex"`;
	}
	# Otherwise, we look inside the dir itself and check if it should played randomly or in order
	else{
	   # If played in order we do not exclude last n files played
	   if($order == 1){@listFiles = `find '$base' -type f | egrep 'mp3|wma|MP3|WMA|ogg|OGG|m4a|M4A'`;}
	   else {
	      # Play randomly and exclude last n files played
	      @listFiles = `find '$base' -type f | egrep 'mp3|wma|MP3|WMA|ogg|OGG|m4a|M4A' | egrep -v "$ex"`;
	      }
	}
	
	# If we have a list that should be played in an order...
	if($order == 1){
	   # put files in order (sort)
	   @listFiles = sort(@listFiles);
	   # a file to store next to be played item number
	   $numFile = $base.'/kolejno.txt';
	   # If the file exist
	   if(-e $numFile){
	      # Read next to be played item number from a file
	      $number = trim(`cat $numFile`);
	      print "Number: $number\n";
	      $numplus = $number+1;
	      # Get number of files in the list
	      $lenList = @listFiles;
	      print "Number of files in the list: $lenList\n";
	      # Start from beginning if the next file would be out of range
	      if($numplus >= $lenList){
		 $numplus = 0;
	      }
	      # Store the next file number to play
	      `echo "$numplus" > $numFile`;
	   }
	   else{
	      # If there's no file we should create one...
	      `echo "1" > $numFile`;
	      print "There's no file for ordered list. Creating one.\n";
	      # ... and play from the beginning
	      $number = "0";
	   }
	   #print @listFiles;
	   # Trim and select a file to play
	   $audiofile = trim($listFiles[$number]);
	}
	else{
	   # If it is not ordered list we should pick a random item
	   # from the list
	   $audiofile = trim($listFiles[rand @listFiles]);
	}
	
	unless(-e $audiofile){
		# The file does not exist. Might got removed while doing other stuff.
		# Let's do a random lookup.
		print "Couldn't find file: ".$audiofile.", using fallback directory.\n";
		@listFiles = `find /500gb/autopilot/$fallback -type f -name '*.mp3'`;
		$audiofile = trim($listFiles[rand @listFiles]);
	}
	
	#$escapedAudiofile = AddSlashes($audiofile);
	
	print `date`."\n";
	
	print "File: ".$audiofile."\n";
	`echo "$audiofile" > /500gb/autopilot/test.txt`;
	# This is an external perl script to get the length of
	# a mp3 file and write it to a tmp file that will be
	# used to sync end time with an external server (it
	# does not have to be localhost)
	$i = `/usr/local/etc/modules/getlen.pl "$audiofile"`;
	
	return $audiofile;
}

sub ices_get_metadata {

	# This one is pretty straight forward...
	# ... but it lacks the ability to run other scripts
	# and some UTF tags are messed up.
	
	#BIGTODO: Fix the UTF problem
	
	$mp3 = MP3::Tag->new($_);
	# Next line does not work... and don't know why
	#$result = `echo "test" >> /500gb/autopilot/test2.txt`;
	binmode DATA, ":utf8";
	binmode STDOUT, ":utf8";
	
	$mp3->get_tags();
	
	($title, $track, $artist, $album, $comment, $year, $genre) = $mp3->autoinfo();
	$comment = $mp3->comment();
	
	$metadata = $artist." - ".$title;

	# If the length of metadata in total is less or equal 3
	# it means no author nor title was set. Return just the filename
	if(length($metadata)<=3){
		$metadata = $audiofile;
		$metadata =~ s/$base//;   
	}
	$mp3->close();

	print "ID Tag: ".$metadata."\n";
	return $metadata;
}

# Function used to put the current line number of
# the playlist in the cue file. If you don't care
# about cue files, just return any integer.
sub ices_get_lineno {
	return 1;
}

return 1;